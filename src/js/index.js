import '../css/style.scss'

const container = document.getElementById('app')

document.getElementById('lodash').onclick = async () => {
  await import(/* webpackChunkName: "lodash" */ 'lodash');
  console.log(_.cloneDeep({ a: 1 }))
}

document.getElementById('user').onclick = async () => {
  const { default: user } = await import(/* webpackChunkName: "user" */ './modules/user');
  console.log(user);
}

document.getElementById('post').onclick = async () => {
  const { default: post } = await import(/* webpackChunkName: "post" */ './modules/post');
  console.log(post);
}