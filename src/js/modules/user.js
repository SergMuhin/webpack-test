import randomInteger from '../utils/randomInteger'

const data = await fetch(`https://jsonplaceholder.typicode.com/users/${randomInteger(1, 10)}`)
const user = await data.json()

export default user