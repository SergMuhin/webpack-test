import randomInteger from '../utils/randomInteger'

const data = await fetch(`https://jsonplaceholder.typicode.com/posts/${randomInteger(1, 10)}`)
const post = await data.json()

export default post