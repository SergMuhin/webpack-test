[![](https://webpack.js.org/favicon.f326220248556af65f41.ico)](https://webpack.js.org)

# Weback test
Webpack test project

## Features
- Different config files for development and production environments
- Code splitting
- Lazy load bundles
- Image minification
- File compression
- ✨Magic ✨

## Installation
- `npm i`
## Development
- `npm run dev`

## Production
- `npm run build`

## Plugins
| Plugin | README |
| ------ | ------ |
| html-webpack-plugin | [https://www.npmjs.com/package/html-webpack-plugin](https://www.npmjs.com/package/html-webpack-plugin) |
| clean-webpack-plugin | [https://www.npmjs.com/package/clean-webpack-plugin](https://www.npmjs.com/package/clean-webpack-plugin) |
| image-minimizer-webpack-plugin | [https://www.npmjs.com/package/image-minimizer-webpack-plugin](https://www.npmjs.com/package/image-minimizer-webpack-plugin) |
| compression-webpack-plugin | [https://www.npmjs.com/package/compression-webpack-plugin](https://www.npmjs.com/package/compression-webpack-plugin) |
