const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');

const htmlWebpackPluginOptions = {
  title: 'Webpack Test',
  meta: { viewport: 'width=device-width, initial-scale=1.0' },
  favicon: path.resolve(__dirname, 'src/favicon.ico'),
  template: path.resolve(__dirname, 'src/template.html'),
  inject: 'body'
}

module.exports = {
  entry: './src/js/index.js',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
        },

      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin(htmlWebpackPluginOptions),
    new CopyPlugin({
      patterns: [
        { from: "./src/img", to: "./img" },
      ],
    })
  ],
  experiments: {
    topLevelAwait: true
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
}

