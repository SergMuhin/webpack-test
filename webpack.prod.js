const { merge } = require('webpack-merge')
const baseConfig = require('./webpack.common.js')
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = merge(baseConfig, {
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.(jpe?g|png)$/i,
        type: "asset",
      },
    ],
  },
  optimization: {
    minimizer: [
      new ImageMinimizerPlugin({
        minimizer: {
          implementation: ImageMinimizerPlugin.squooshMinify,
        },
      }),
    ],
  },
  plugins: [
    new CompressionPlugin()
  ]
})